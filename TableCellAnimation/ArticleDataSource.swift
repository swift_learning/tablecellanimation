//
//  ArticleDataSource.swift
//  TableCellAnimation
//
//  Created by iulian david on 04/10/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

class ArticleDataSource: NSObject, UITableViewDataSource {

    let postTitles = ["Use Background Transfer Service To Download File in Background",
    "First Time App Developer Success Stories Part 1",
    "Adding Animated Effects to iOS App Using UIKit Dynamics",
    "Working with Game Center and Game Kit Framework",
    "How to Access iOS Calendar, Events and Reminders",
    "Creating Circular Profile Image"]
    let postImages = ["bts.jpg", "first-time-developer.jpg", "uidynamics.jpg", "gamecenter.jpg", "event-kit.jpg", "circular-image.jpg"]

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postTitles.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? ArticleTableViewCell else { fatalError("Incorrect Table Configuration") }

        cell.titleLabel.text = postTitles[indexPath.row]
        cell.postImageView.image = UIImage(named: postImages[indexPath.row])

        return cell
    }

}
