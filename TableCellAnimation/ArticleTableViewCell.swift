//
//  ArticleTableViewCell.swift
//  TableCellAnimation
//
//  Created by iulian david on 04/10/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
}
