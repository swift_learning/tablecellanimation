//
//  ArticleTableViewController.swift
//  TableCellAnimation
//
//  Created by iulian david on 04/10/2019.
//  Copyright © 2019 iulian david. All rights reserved.
//

import UIKit

class ArticleTableViewController: UITableViewController {

    let datasource = ArticleDataSource()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = datasource
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        // Define the initial state (Before the animation
//        cell.alpha = 0
//
//        //Define the final state (After the animation)
//        UIView.animate(withDuration: 1.0) {
//            cell.alpha = 1.0
//        }

        // Define the initial state (Before the animation
//        let rotationAngleInRadians = 90.0 * CGFloat(.pi/180.0)
//        let rotationTransform = CATransform3DMakeRotation(rotationAngleInRadians, 0, 0, 1)
        let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, -500, 100, 0)
        cell.layer.transform = rotationTransform

        //Define the final state
        UIView.animate(withDuration: 1.0) {
            cell.layer.transform = CATransform3DIdentity
        }
    }
}
